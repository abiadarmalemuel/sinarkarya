﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Dim PushTransition1 As DevExpress.Utils.Animation.PushTransition = New DevExpress.Utils.Animation.PushTransition()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Me.RepositoryItemProgressBar1 = New DevExpress.XtraEditors.Repository.RepositoryItemProgressBar()
        Me.RepositoryItemSearchControl1 = New DevExpress.XtraEditors.Repository.RepositoryItemSearchControl()
        Me.RibbonControl1 = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.PesananPejualanbut = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.PengirimanPenjualanbut = New DevExpress.XtraBars.BarButtonItem()
        Me.FakturPenjualanbut = New DevExpress.XtraBars.BarButtonItem()
        Me.SkinRibbonGalleryBarItem1 = New DevExpress.XtraBars.SkinRibbonGalleryBarItem()
        Me.BarEditItem1 = New DevExpress.XtraBars.BarEditItem()
        Me.BarButtonGroup1 = New DevExpress.XtraBars.BarButtonGroup()
        Me.BarWorkspaceMenuItem1 = New DevExpress.XtraBars.BarWorkspaceMenuItem()
        Me.WorkspaceManager1 = New DevExpress.Utils.WorkspaceManager()
        Me.RibbonGalleryBarItem1 = New DevExpress.XtraBars.RibbonGalleryBarItem()
        Me.PesananPembelianbut = New DevExpress.XtraBars.BarButtonItem()
        Me.PenerimaanBarangbut = New DevExpress.XtraBars.BarButtonItem()
        Me.FakturPembelianbut = New DevExpress.XtraBars.BarButtonItem()
        Me.SkinRibbonGalleryBarItem2 = New DevExpress.XtraBars.SkinRibbonGalleryBarItem()
        Me.pelangganbut = New DevExpress.XtraBars.BarButtonItem()
        Me.pemasokbut = New DevExpress.XtraBars.BarButtonItem()
        Me.barangbut = New DevExpress.XtraBars.BarButtonItem()
        Me.BarEditItem2 = New DevExpress.XtraBars.BarEditItem()
        Me.SkinRibbonGalleryBarItem3 = New DevExpress.XtraBars.SkinRibbonGalleryBarItem()
        Me.SkinRibbonGalleryBarItem4 = New DevExpress.XtraBars.SkinRibbonGalleryBarItem()
        Me.PenawaranPenjualanbut = New DevExpress.XtraBars.BarButtonItem()
        Me.ReturPenjualanbut = New DevExpress.XtraBars.BarButtonItem()
        Me.PermintaanPembelianbut = New DevExpress.XtraBars.BarButtonItem()
        Me.PenerimaanPenjualanbut = New DevExpress.XtraBars.BarButtonItem()
        Me.PembayaranPembelianbut = New DevExpress.XtraBars.BarButtonItem()
        Me.PembayranPembelianbut = New DevExpress.XtraBars.BarButtonItem()
        Me.ReturPembelianbut = New DevExpress.XtraBars.BarButtonItem()
        Me.JadwalLayananbut = New DevExpress.XtraBars.BarButtonItem()
        Me.Pengaduanbut = New DevExpress.XtraBars.BarButtonItem()
        Me.BarEditItem3 = New DevExpress.XtraBars.BarEditItem()
        Me.RepositoryItemButtonEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.SkinRibbonGalleryBarItem5 = New DevExpress.XtraBars.SkinRibbonGalleryBarItem()
        Me.Master = New DevExpress.XtraBars.Ribbon.RibbonPageCategory()
        Me.RibbonPage3 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup3 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.Modules = New DevExpress.XtraBars.Ribbon.RibbonPageCategory()
        Me.RibbonPage1 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage2 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage4 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup4 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonStatusBar1 = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
        Me.ApplicationMenu1 = New DevExpress.XtraBars.Ribbon.ApplicationMenu(Me.components)
        Me.XtraUserControl1 = New DevExpress.XtraEditors.XtraUserControl()
        Me.XtraTabbedMdiManager1 = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        CType(Me.RepositoryItemProgressBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemSearchControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RibbonControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemButtonEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ApplicationMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RepositoryItemProgressBar1
        '
        Me.RepositoryItemProgressBar1.Name = "RepositoryItemProgressBar1"
        '
        'RepositoryItemSearchControl1
        '
        Me.RepositoryItemSearchControl1.AutoHeight = False
        Me.RepositoryItemSearchControl1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Repository.ClearButton(), New DevExpress.XtraEditors.Repository.SearchButton()})
        Me.RepositoryItemSearchControl1.Name = "RepositoryItemSearchControl1"
        '
        'RibbonControl1
        '
        Me.RibbonControl1.AllowCustomization = True
        Me.RibbonControl1.AllowInplaceLinks = True
        Me.RibbonControl1.ExpandCollapseItem.Id = 0
        Me.RibbonControl1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl1.ExpandCollapseItem, Me.PesananPejualanbut, Me.BarButtonItem3, Me.PengirimanPenjualanbut, Me.FakturPenjualanbut, Me.SkinRibbonGalleryBarItem1, Me.BarEditItem1, Me.BarButtonGroup1, Me.BarWorkspaceMenuItem1, Me.RibbonGalleryBarItem1, Me.PesananPembelianbut, Me.PenerimaanBarangbut, Me.FakturPembelianbut, Me.SkinRibbonGalleryBarItem2, Me.pelangganbut, Me.pemasokbut, Me.barangbut, Me.BarEditItem2, Me.SkinRibbonGalleryBarItem3, Me.SkinRibbonGalleryBarItem4, Me.PenawaranPenjualanbut, Me.ReturPenjualanbut, Me.PermintaanPembelianbut, Me.PenerimaanPenjualanbut, Me.PembayaranPembelianbut, Me.PembayranPembelianbut, Me.ReturPembelianbut, Me.JadwalLayananbut, Me.Pengaduanbut, Me.BarEditItem3, Me.SkinRibbonGalleryBarItem5})
        Me.RibbonControl1.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl1.MaxItemId = 1
        Me.RibbonControl1.Name = "RibbonControl1"
        Me.RibbonControl1.PageCategories.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageCategory() {Me.Master, Me.Modules})
        Me.RibbonControl1.PopupMenuAlignment = DevExpress.XtraBars.PopupMenuAlignment.Left
        Me.RibbonControl1.PopupShowMode = DevExpress.XtraBars.PopupShowMode.Inplace
        Me.RibbonControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemButtonEdit1})
        Me.RibbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013
        Me.RibbonControl1.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.[True]
        Me.RibbonControl1.Size = New System.Drawing.Size(1350, 135)
        Me.RibbonControl1.StatusBar = Me.RibbonStatusBar1
        Me.RibbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden
        '
        'PesananPejualanbut
        '
        Me.PesananPejualanbut.Caption = "Pesanan Penjualan"
        Me.PesananPejualanbut.Glyph = Global.ProjectN.My.Resources.Resources.bocontact_32x32
        Me.PesananPejualanbut.Id = 6
        Me.PesananPejualanbut.LargeWidth = 100
        Me.PesananPejualanbut.Name = "PesananPejualanbut"
        Me.PesananPejualanbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left
        Me.BarButtonItem3.Caption = "BarButtonItem3"
        Me.BarButtonItem3.Id = 7
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'PengirimanPenjualanbut
        '
        Me.PengirimanPenjualanbut.Caption = "Pengiriman Barang"
        Me.PengirimanPenjualanbut.Glyph = CType(resources.GetObject("PengirimanPenjualanbut.Glyph"), System.Drawing.Image)
        Me.PengirimanPenjualanbut.Id = 1
        Me.PengirimanPenjualanbut.LargeWidth = 100
        Me.PengirimanPenjualanbut.Name = "PengirimanPenjualanbut"
        Me.PengirimanPenjualanbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'FakturPenjualanbut
        '
        Me.FakturPenjualanbut.Caption = "Faktur Penjualan"
        Me.FakturPenjualanbut.Glyph = Global.ProjectN.My.Resources.Resources.printer_32x32
        Me.FakturPenjualanbut.Id = 2
        Me.FakturPenjualanbut.LargeWidth = 100
        Me.FakturPenjualanbut.Name = "FakturPenjualanbut"
        Me.FakturPenjualanbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'SkinRibbonGalleryBarItem1
        '
        Me.SkinRibbonGalleryBarItem1.Caption = "SkinRibbonGalleryBarItem1"
        Me.SkinRibbonGalleryBarItem1.Id = 4
        Me.SkinRibbonGalleryBarItem1.Name = "SkinRibbonGalleryBarItem1"
        '
        'BarEditItem1
        '
        Me.BarEditItem1.Caption = "BarEditItem1"
        Me.BarEditItem1.Edit = Me.RepositoryItemProgressBar1
        Me.BarEditItem1.Id = 5
        Me.BarEditItem1.Name = "BarEditItem1"
        '
        'BarButtonGroup1
        '
        Me.BarButtonGroup1.Caption = "BarButtonGroup1"
        Me.BarButtonGroup1.Id = 6
        Me.BarButtonGroup1.Name = "BarButtonGroup1"
        '
        'BarWorkspaceMenuItem1
        '
        Me.BarWorkspaceMenuItem1.Caption = "BarWorkspaceMenuItem1"
        Me.BarWorkspaceMenuItem1.Id = 7
        Me.BarWorkspaceMenuItem1.Name = "BarWorkspaceMenuItem1"
        Me.BarWorkspaceMenuItem1.WorkspaceManager = Me.WorkspaceManager1
        '
        'WorkspaceManager1
        '
        Me.WorkspaceManager1.TargetControl = Me
        Me.WorkspaceManager1.TransitionType = PushTransition1
        '
        'RibbonGalleryBarItem1
        '
        Me.RibbonGalleryBarItem1.Caption = "RibbonGalleryBarItem1"
        Me.RibbonGalleryBarItem1.Id = 8
        Me.RibbonGalleryBarItem1.Name = "RibbonGalleryBarItem1"
        '
        'PesananPembelianbut
        '
        Me.PesananPembelianbut.Caption = "Pesanan Pembelian"
        Me.PesananPembelianbut.Glyph = CType(resources.GetObject("PesananPembelianbut.Glyph"), System.Drawing.Image)
        Me.PesananPembelianbut.Id = 9
        Me.PesananPembelianbut.LargeWidth = 100
        Me.PesananPembelianbut.Name = "PesananPembelianbut"
        Me.PesananPembelianbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'PenerimaanBarangbut
        '
        Me.PenerimaanBarangbut.Caption = "Penerimaan Barang"
        Me.PenerimaanBarangbut.Glyph = CType(resources.GetObject("PenerimaanBarangbut.Glyph"), System.Drawing.Image)
        Me.PenerimaanBarangbut.Id = 10
        Me.PenerimaanBarangbut.LargeWidth = 100
        Me.PenerimaanBarangbut.Name = "PenerimaanBarangbut"
        Me.PenerimaanBarangbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'FakturPembelianbut
        '
        Me.FakturPembelianbut.Caption = "Faktur Pembelian"
        Me.FakturPembelianbut.Glyph = CType(resources.GetObject("FakturPembelianbut.Glyph"), System.Drawing.Image)
        Me.FakturPembelianbut.Id = 11
        Me.FakturPembelianbut.LargeWidth = 100
        Me.FakturPembelianbut.Name = "FakturPembelianbut"
        Me.FakturPembelianbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'SkinRibbonGalleryBarItem2
        '
        Me.SkinRibbonGalleryBarItem2.Caption = "SkinRibbonGalleryBarItem2"
        Me.SkinRibbonGalleryBarItem2.Id = 12
        Me.SkinRibbonGalleryBarItem2.Name = "SkinRibbonGalleryBarItem2"
        '
        'pelangganbut
        '
        Me.pelangganbut.Caption = "Pelanggan"
        Me.pelangganbut.DropDownEnabled = False
        Me.pelangganbut.Glyph = CType(resources.GetObject("pelangganbut.Glyph"), System.Drawing.Image)
        Me.pelangganbut.Id = 1
        Me.pelangganbut.LargeWidth = 100
        Me.pelangganbut.Name = "pelangganbut"
        Me.pelangganbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'pemasokbut
        '
        Me.pemasokbut.Caption = "Pemasok"
        Me.pemasokbut.Glyph = Global.ProjectN.My.Resources.Resources.supplier1600
        Me.pemasokbut.Id = 2
        Me.pemasokbut.LargeWidth = 100
        Me.pemasokbut.Name = "pemasokbut"
        Me.pemasokbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'barangbut
        '
        Me.barangbut.Caption = "Barang"
        Me.barangbut.Glyph = CType(resources.GetObject("barangbut.Glyph"), System.Drawing.Image)
        Me.barangbut.Id = 3
        Me.barangbut.LargeGlyph = CType(resources.GetObject("barangbut.LargeGlyph"), System.Drawing.Image)
        Me.barangbut.LargeWidth = 100
        Me.barangbut.Name = "barangbut"
        Me.barangbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'BarEditItem2
        '
        Me.BarEditItem2.Caption = "BarEditItem2"
        Me.BarEditItem2.Edit = Me.RepositoryItemSearchControl1
        Me.BarEditItem2.Id = 4
        Me.BarEditItem2.Name = "BarEditItem2"
        '
        'SkinRibbonGalleryBarItem3
        '
        Me.SkinRibbonGalleryBarItem3.Caption = "SkinRibbonGalleryBarItem3"
        Me.SkinRibbonGalleryBarItem3.Id = 1
        Me.SkinRibbonGalleryBarItem3.Name = "SkinRibbonGalleryBarItem3"
        '
        'SkinRibbonGalleryBarItem4
        '
        Me.SkinRibbonGalleryBarItem4.Caption = "SkinRibbonGalleryBarItem4"
        Me.SkinRibbonGalleryBarItem4.Id = 2
        Me.SkinRibbonGalleryBarItem4.Name = "SkinRibbonGalleryBarItem4"
        '
        'PenawaranPenjualanbut
        '
        Me.PenawaranPenjualanbut.Caption = "Penawaran Penjualan"
        Me.PenawaranPenjualanbut.Glyph = Global.ProjectN.My.Resources.Resources.product_sales_report
        Me.PenawaranPenjualanbut.Id = 1
        Me.PenawaranPenjualanbut.LargeGlyph = CType(resources.GetObject("PenawaranPenjualanbut.LargeGlyph"), System.Drawing.Image)
        Me.PenawaranPenjualanbut.LargeWidth = 100
        Me.PenawaranPenjualanbut.Name = "PenawaranPenjualanbut"
        Me.PenawaranPenjualanbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'ReturPenjualanbut
        '
        Me.ReturPenjualanbut.Caption = "Retur Penjualan"
        Me.ReturPenjualanbut.Glyph = CType(resources.GetObject("ReturPenjualanbut.Glyph"), System.Drawing.Image)
        Me.ReturPenjualanbut.Id = 2
        Me.ReturPenjualanbut.LargeWidth = 100
        Me.ReturPenjualanbut.Name = "ReturPenjualanbut"
        Me.ReturPenjualanbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'PermintaanPembelianbut
        '
        Me.PermintaanPembelianbut.Caption = "Permintaan Pembelian"
        Me.PermintaanPembelianbut.Glyph = CType(resources.GetObject("PermintaanPembelianbut.Glyph"), System.Drawing.Image)
        Me.PermintaanPembelianbut.Id = 3
        Me.PermintaanPembelianbut.LargeWidth = 100
        Me.PermintaanPembelianbut.Name = "PermintaanPembelianbut"
        Me.PermintaanPembelianbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'PenerimaanPenjualanbut
        '
        Me.PenerimaanPenjualanbut.Caption = "Penerimaan Penjualan"
        Me.PenerimaanPenjualanbut.Glyph = CType(resources.GetObject("PenerimaanPenjualanbut.Glyph"), System.Drawing.Image)
        Me.PenerimaanPenjualanbut.Id = 4
        Me.PenerimaanPenjualanbut.LargeWidth = 100
        Me.PenerimaanPenjualanbut.Name = "PenerimaanPenjualanbut"
        Me.PenerimaanPenjualanbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'PembayaranPembelianbut
        '
        Me.PembayaranPembelianbut.Caption = "Pembayaran Pembelian"
        Me.PembayaranPembelianbut.Id = 5
        Me.PembayaranPembelianbut.Name = "PembayaranPembelianbut"
        Me.PembayaranPembelianbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        ToolTipItem1.Appearance.Image = Global.ProjectN.My.Resources.Resources.bocustomer_16x16
        ToolTipItem1.Appearance.Options.UseImage = True
        ToolTipItem1.Image = Global.ProjectN.My.Resources.Resources.bocustomer_16x16
        SuperToolTip1.Items.Add(ToolTipItem1)
        Me.PembayaranPembelianbut.SuperTip = SuperToolTip1
        '
        'PembayranPembelianbut
        '
        Me.PembayranPembelianbut.Caption = "Pembayaran Pembelian"
        Me.PembayranPembelianbut.Glyph = CType(resources.GetObject("PembayranPembelianbut.Glyph"), System.Drawing.Image)
        Me.PembayranPembelianbut.Id = 6
        Me.PembayranPembelianbut.LargeGlyph = CType(resources.GetObject("PembayranPembelianbut.LargeGlyph"), System.Drawing.Image)
        Me.PembayranPembelianbut.LargeWidth = 100
        Me.PembayranPembelianbut.Name = "PembayranPembelianbut"
        '
        'ReturPembelianbut
        '
        Me.ReturPembelianbut.Caption = "ReturPembelian"
        Me.ReturPembelianbut.Glyph = CType(resources.GetObject("ReturPembelianbut.Glyph"), System.Drawing.Image)
        Me.ReturPembelianbut.Id = 7
        Me.ReturPembelianbut.LargeWidth = 100
        Me.ReturPembelianbut.Name = "ReturPembelianbut"
        Me.ReturPembelianbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'JadwalLayananbut
        '
        Me.JadwalLayananbut.Caption = "Jadwal Layanan"
        Me.JadwalLayananbut.Glyph = CType(resources.GetObject("JadwalLayananbut.Glyph"), System.Drawing.Image)
        Me.JadwalLayananbut.Id = 8
        Me.JadwalLayananbut.LargeWidth = 100
        Me.JadwalLayananbut.Name = "JadwalLayananbut"
        Me.JadwalLayananbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'Pengaduanbut
        '
        Me.Pengaduanbut.Caption = "Pengaduan"
        Me.Pengaduanbut.Glyph = CType(resources.GetObject("Pengaduanbut.Glyph"), System.Drawing.Image)
        Me.Pengaduanbut.Id = 9
        Me.Pengaduanbut.LargeWidth = 100
        Me.Pengaduanbut.Name = "Pengaduanbut"
        Me.Pengaduanbut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'BarEditItem3
        '
        Me.BarEditItem3.Caption = "BarEditItem3"
        Me.BarEditItem3.Edit = Me.RepositoryItemButtonEdit1
        Me.BarEditItem3.Id = 10
        Me.BarEditItem3.Name = "BarEditItem3"
        '
        'RepositoryItemButtonEdit1
        '
        Me.RepositoryItemButtonEdit1.AutoHeight = False
        Me.RepositoryItemButtonEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemButtonEdit1.Name = "RepositoryItemButtonEdit1"
        '
        'SkinRibbonGalleryBarItem5
        '
        Me.SkinRibbonGalleryBarItem5.Caption = "SkinRibbonGalleryBarItem5"
        Me.SkinRibbonGalleryBarItem5.Id = 11
        Me.SkinRibbonGalleryBarItem5.Name = "SkinRibbonGalleryBarItem5"
        '
        'Master
        '
        Me.Master.AutoStretchPageHeaders = True
        Me.Master.Name = "Master"
        Me.Master.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.RibbonPage3})
        Me.Master.Text = "Master"
        '
        'RibbonPage3
        '
        Me.RibbonPage3.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup3})
        Me.RibbonPage3.Image = CType(resources.GetObject("RibbonPage3.Image"), System.Drawing.Image)
        Me.RibbonPage3.Name = "RibbonPage3"
        Me.RibbonPage3.Text = "Master"
        '
        'RibbonPageGroup3
        '
        Me.RibbonPageGroup3.ItemLinks.Add(Me.pelangganbut)
        Me.RibbonPageGroup3.ItemLinks.Add(Me.pemasokbut)
        Me.RibbonPageGroup3.ItemLinks.Add(Me.barangbut)
        Me.RibbonPageGroup3.Name = "RibbonPageGroup3"
        Me.RibbonPageGroup3.ShowCaptionButton = False
        '
        'Modules
        '
        Me.Modules.AutoStretchPageHeaders = True
        Me.Modules.Name = "Modules"
        Me.Modules.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.RibbonPage1, Me.RibbonPage2, Me.RibbonPage4})
        Me.Modules.Text = "Modules"
        '
        'RibbonPage1
        '
        Me.RibbonPage1.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup1})
        Me.RibbonPage1.Image = Global.ProjectN.My.Resources.Resources.bosale_32x32
        Me.RibbonPage1.Name = "RibbonPage1"
        Me.RibbonPage1.Text = "Penjualan"
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.Glyph = Global.ProjectN.My.Resources.Resources.bocustomer_32x32
        Me.RibbonPageGroup1.ItemLinks.Add(Me.PenawaranPenjualanbut)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.PesananPejualanbut)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.PengirimanPenjualanbut)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.PenerimaanPenjualanbut)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.FakturPenjualanbut)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.ReturPenjualanbut)
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.ShowCaptionButton = False
        '
        'RibbonPage2
        '
        Me.RibbonPage2.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup2})
        Me.RibbonPage2.Image = Global.ProjectN.My.Resources.Resources.boorder_32x32
        Me.RibbonPage2.Name = "RibbonPage2"
        Me.RibbonPage2.Text = "Pembelian"
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.PermintaanPembelianbut)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.PesananPembelianbut)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.PenerimaanBarangbut)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.FakturPembelianbut)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.PembayranPembelianbut)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.ReturPembelianbut)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.ShowCaptionButton = False
        '
        'RibbonPage4
        '
        Me.RibbonPage4.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup4})
        Me.RibbonPage4.Image = CType(resources.GetObject("RibbonPage4.Image"), System.Drawing.Image)
        Me.RibbonPage4.Name = "RibbonPage4"
        Me.RibbonPage4.Text = "Layanan"
        '
        'RibbonPageGroup4
        '
        Me.RibbonPageGroup4.ItemLinks.Add(Me.Pengaduanbut)
        Me.RibbonPageGroup4.ItemLinks.Add(Me.JadwalLayananbut)
        Me.RibbonPageGroup4.Name = "RibbonPageGroup4"
        Me.RibbonPageGroup4.ShowCaptionButton = False
        '
        'RibbonStatusBar1
        '
        Me.RibbonStatusBar1.Location = New System.Drawing.Point(0, 702)
        Me.RibbonStatusBar1.Name = "RibbonStatusBar1"
        Me.RibbonStatusBar1.Ribbon = Me.RibbonControl1
        Me.RibbonStatusBar1.Size = New System.Drawing.Size(1350, 27)
        '
        'ApplicationMenu1
        '
        Me.ApplicationMenu1.Name = "ApplicationMenu1"
        Me.ApplicationMenu1.Ribbon = Me.RibbonControl1
        '
        'XtraUserControl1
        '
        Me.XtraUserControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraUserControl1.Location = New System.Drawing.Point(1, 1)
        Me.XtraUserControl1.Name = "XtraUserControl1"
        Me.XtraUserControl1.Size = New System.Drawing.Size(1344, 561)
        Me.XtraUserControl1.TabIndex = 0
        '
        'XtraTabbedMdiManager1
        '
        Me.XtraTabbedMdiManager1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.XtraTabbedMdiManager1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPagesAndTabControlHeader
        Me.XtraTabbedMdiManager1.FloatOnDrag = DevExpress.Utils.DefaultBoolean.[True]
        Me.XtraTabbedMdiManager1.FloatPageDragMode = DevExpress.XtraTabbedMdi.FloatPageDragMode.Preview
        Me.XtraTabbedMdiManager1.HeaderButtonsShowMode = DevExpress.XtraTab.TabButtonShowMode.Always
        Me.XtraTabbedMdiManager1.MdiParent = Me
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1350, 729)
        Me.Controls.Add(Me.RibbonControl1)
        Me.Controls.Add(Me.RibbonStatusBar1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.IsMdiContainer = True
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "100"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RepositoryItemProgressBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemSearchControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RibbonControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemButtonEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ApplicationMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents RibbonControl1 As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents RibbonStatusBar1 As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Friend WithEvents Modules As DevExpress.XtraBars.Ribbon.RibbonPageCategory
    Friend WithEvents ApplicationMenu1 As DevExpress.XtraBars.Ribbon.ApplicationMenu
    Friend WithEvents RibbonPage1 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPage2 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents PesananPejualanbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PengirimanPenjualanbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents FakturPenjualanbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents SkinRibbonGalleryBarItem1 As DevExpress.XtraBars.SkinRibbonGalleryBarItem
    Friend WithEvents BarEditItem1 As DevExpress.XtraBars.BarEditItem
    Friend WithEvents BarButtonGroup1 As DevExpress.XtraBars.BarButtonGroup
    Friend WithEvents BarWorkspaceMenuItem1 As DevExpress.XtraBars.BarWorkspaceMenuItem
    Friend WithEvents RibbonGalleryBarItem1 As DevExpress.XtraBars.RibbonGalleryBarItem
    Friend WithEvents PesananPembelianbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PenerimaanBarangbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents FakturPembelianbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents SkinRibbonGalleryBarItem2 As DevExpress.XtraBars.SkinRibbonGalleryBarItem
    Friend WithEvents XtraUserControl1 As DevExpress.XtraEditors.XtraUserControl
    Friend WithEvents WorkspaceManager1 As DevExpress.Utils.WorkspaceManager
    Friend WithEvents Master As DevExpress.XtraBars.Ribbon.RibbonPageCategory
    Friend WithEvents RibbonPage3 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents pelangganbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents pemasokbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup3 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents barangbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents XtraTabbedMdiManager1 As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Friend WithEvents BarEditItem2 As DevExpress.XtraBars.BarEditItem
    Friend WithEvents SkinRibbonGalleryBarItem3 As DevExpress.XtraBars.SkinRibbonGalleryBarItem
    Friend WithEvents SkinRibbonGalleryBarItem4 As DevExpress.XtraBars.SkinRibbonGalleryBarItem
    Friend WithEvents RepositoryItemProgressBar1 As DevExpress.XtraEditors.Repository.RepositoryItemProgressBar
    Friend WithEvents RepositoryItemSearchControl1 As DevExpress.XtraEditors.Repository.RepositoryItemSearchControl
    Friend WithEvents PenawaranPenjualanbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ReturPenjualanbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PermintaanPembelianbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PenerimaanPenjualanbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PembayaranPembelianbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PembayranPembelianbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ReturPembelianbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents JadwalLayananbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Pengaduanbut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPage4 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup4 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarEditItem3 As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemButtonEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents SkinRibbonGalleryBarItem5 As DevExpress.XtraBars.SkinRibbonGalleryBarItem
End Class
